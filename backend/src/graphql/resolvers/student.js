const Job = require('../../models/Job');
var express = require('express');

module.exports = {
    searchJobs: async (args, context) => {
        try {
          const jobs = await Job.find();
          return jobs.filter(job => job.title.includes(args.title)).map(job => {
              return {
                ...job._doc,
                posting_date: new Date(job._doc.posting_date).toISOString().split('T')[0],
		            app_deadline: new Date(job._doc.app_deadline).toISOString().split('T')[0]
              }
          });
        } catch (e) {
            return new Error('Unable to fetch data.');
        }
    },
    updateStudentProfile: async (args, context) => {
        try {
          const input = args.studentInput;
          const student = await Student.findById(context.req.user._id);
          student.skills.push(input.skill);
          const result = await student.save();
          return { ...result._doc };
        } catch (e) {
          return new Error('Unable to update the student. Please try again.');
        }
    },
    getAllStudents: async (args, context) => {
        try {
            const students = await Student.find({ _id: {'$ne': context.req.user._id}});
            return students;
        } catch (e) {
            return new Error('Unable to fetch data.');
        }
    },
    applyToJob: async (args, context) => {
        const { jobId, resume } = args.applyJobInput;
        const job = await Job.findById(jobId);
        const student = context.req.user._id;
        try {
          const application = {
            student,
            resume,
            applied_date: new Date(),
            status: 'Pending'
          };
          job.application.unshift(application);
          await job.save();
          application.applied_date = new Date(application.applied_date).toISOString().split('T')[0];
          return application;
        } catch (e) {
          return new Error('Unable to apply job. Please try again.');
        }
    }, 
};
