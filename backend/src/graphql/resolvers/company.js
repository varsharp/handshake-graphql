/* eslint-disable no-underscore-dangle */

const Job = require('../../models/Job');
var express = require('express');

module.exports = {
  postJob: async (args, context) => {
    try {
      const jobs = args.jobInput;

      const jobEntry = new Job({
        ...jobs,
        company: context.req.user._id,
        posting_date: new Date()
      });
      const result = await jobEntry.save();
      result._doc.posting_date = new Date(result._doc.posting_date).toISOString().split('T')[0];
	    return { ...result._doc };
    } catch (e) {
        return new Error('Unable to post a job. Please try again.');
    }
  },
  getJobApplicants: async (args, context) => {
    try {
      const applications = await Job.find({ _id: args.jobId }).select('application -_id');
      const students = [];
  
      if (applications && applications[0]) {
          for( const app of applications[0].application ) {
              let student = await Student.find({ _id: app.student });
              students.push(student[0]);
          }
          
          if(students && students.length > 0) {
            return students;
          }
      }
      return students ;
    } catch (e) {
        return new Error('Unable to fetch data.');
    }
  },
  getStudents: async (args, context) => {
    try {
      const students = await Student.find();
      return students;
    } catch (e) {
       return new Error('Unable to fetch data.');
    }
  },
  updateCompanyProfile: async(args, context) => {
    try {
      const input = args.companyInput;
      const company = await Company.findById(context.req.user._id);      
      company.city = input.city;
      company.state = input.state;
      company.country = input.country;
      const result = await company.save();
      return { ...result._doc };
    } catch (e) {
      console.log(e)
      return new Error('Unable to update the company description. Please try again.');
    }
  },
};
