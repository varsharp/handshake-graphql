const studentAuthResolver = require('./auth');
const companyAuthResolver = require('./companyAuth');
const companyResolver = require('./company');
const studentResolver = require('./student');

const rootResolver = {
  ...studentAuthResolver,
  ...companyAuthResolver,
  ...companyResolver,
  ...studentResolver,
};

module.exports = rootResolver;