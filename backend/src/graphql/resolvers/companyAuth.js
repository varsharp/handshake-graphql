/* eslint-disable no-underscore-dangle */
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const config = require('../../../config');

const Company = require('../../models/Company');

module.exports = {
  companySignUp: async (args) => {
    try {
      let userCompany = await Company.findOne({
        email: args.companySignUpInput.email,
      });

      if (userCompany) {
        return new Error('Email already exists! Please sign in or create a new account.');
      }
      userCompany = new Company({
        ...args.companySignUpInput,
      });
      const salt = await bcrypt.genSalt(10);

      userCompany.password = await bcrypt.hash(userCompany.password, salt);
      const result = await userCompany.save();
      return { ...result._doc };
    } catch (e) {
      return new Error('Unable to sign up. Please try again.');
    }
  },
  companyLogin: async (args, context) => {
    try {
      const { email, password } = args.companyLoginInput;
      const userCompany = await Company.findOne({
        email,
      });
      if (!userCompany) {
        return new Error('User not found.');
      }

      const isMatch = await bcrypt.compare(password.toString(), userCompany.password);
      if (!isMatch) {
        return new Error('Invalid Credentials. Please try again.');
      }
      const payload = {
        user: {
          _id: userCompany._id,
          email: userCompany.email,
          first_name: userCompany.name,
        },
      };
      const token = jwt.sign(
        payload,
        config.JWTPASSWORD,
        {
          expiresIn: 360000,
        }
      );
      context.res.cookie('token', token, { maxAge: 900000, httpOnly: false, path: '/' });
      return { userId: userCompany._id, token, tokenExpiration: 360000 };
    } catch (e) {
      return new Error('Unable to log in. Please try again.');
    }
  },
};;