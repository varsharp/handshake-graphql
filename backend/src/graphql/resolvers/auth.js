/* eslint-disable no-underscore-dangle */
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const config = require('../../../config');

const Student = require('../../models/Student');

module.exports = {
  studentSignUp: async (args) => {
    try {
      let userStudent = await Student.findOne({
        email: args.studentInput.email,
      });

      if (userStudent) {
        return new Error('Email already exists! Please sign in or create a new account.');
      }
      userStudent = new Student({
        ...args.studentInput,
      });
      const salt = await bcrypt.genSalt(10);

      userStudent.password = await bcrypt.hash(userStudent.password, salt);
      const result = await userStudent.save();
      return { ...result._doc };
    } catch (e) {
      return new Error('Unable to sign up. Please try again.');
    }
  },
  studentLogin: async (args, context) => {
    const { email, password } = args.studentLoginInput;
    try {
      const userStudent = await Student.findOne({
        email,
      });
      if (!userStudent) {
        return new Error('User not found.');
      }

      const isMatch = await bcrypt.compare(password.toString(), userStudent.password);
      if (!isMatch) {
        return new Error('Invalid Credentials. Please try again.');
      }
      const payload = {
        user: {
          _id: userStudent._id,
          email: userStudent.email,
          first_name: userStudent.first_name,
          user: userStudent.user,
        },
      };
      const token = jwt.sign(
        payload,
        config.JWTPASSWORD,
        {
          expiresIn: 360000,
        },
      );
      context.res.cookie('token', token, { maxAge: 900000, httpOnly: false, path: '/' });
      return { userId: userStudent._id, token, tokenExpiration: 360000 };
    } catch (e) {
      return new Error('Unable to log in. Please try again.');
    }
  },
};