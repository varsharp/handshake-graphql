const { buildSchema } = require('graphql');

module.exports = buildSchema(`
type Education {
  _id: ID!
  college_name: String!
  degree: String!
  cgpa: Float
  major: String!
  year_of_passing: Int
  city: String
  state: String
}

type Experience {
  _id: ID!
  title: String!
  company: String!
  start_date: String!
  end_date: String!
  description: String
  city: String
  state: String
}

type Student {
  _id: ID!
  first_name: String!
  last_name: String!
  email: String!
  password: String!
  college_name: String!
  career_objective: String
  skills: [String!]!
  profile_picture: String
  user: String!
  education: [Education!]!
  experience: [Experience!]!
}

type AuthData {
  userId: ID
  token: String
  tokenExpiration: Int
}

input StudentSignUp {
  first_name: String!
  last_name: String!
  email: String!
  password: String!
  college_name: String!
}

type Company {
    _id: ID!
    name: String!
    email: String!
    password: String!
    city: String!
    state: String!
    country: String!
    profile_picture: String!
    phone_number: String
}

input CompanySignUp {
    name: String!
    email: String!
    password: String!
    city: String!
    state: String!
    country: String!
}

input CompanyInput {
    name: String
    email: String
    password: String
    city: String
    state: String
    country: String
    profile_picture: String
    phone_number: String
}

input StudentInput {
    first_name: String
    last_name: String
    email: String
    college_name: String
    career_objective: String
    skill: String
    profile_picture: String
}

input UserLogin {
    email: String!
    password: String!
}  

input StudentLogin {
  email: String!
  password: String!
}

input JobPosting {
    title: String!
    posting_date: String
    app_deadline: String
    location: String
    salary: Float
    description: String
    job_type: String
}

type Job {
    _id: ID!
    title: String!
    posting_date: String
    app_deadline: String
    location: String!
    salary: Float
    description: String
    job_type: String!
    company: ID!
}

input JobApplication {
    jobId: String!
    resume: String!
}

type Application {
    _id: ID
    student: ID!
    applied_date: String!
    status: String
}

type RootQuery {
  getJobApplicants(jobId: String!): [Student]!
  getStudents: [Student]!
  getAllStudents: [Student]!
  searchJobs(title: String!): [Job]!
}

type RootMutation {
  studentLogin(studentLoginInput: StudentLogin!): AuthData!
  companyLogin(companyLoginInput: UserLogin!): AuthData!
  studentSignUp(studentInput: StudentSignUp): Student
  companySignUp(companySignUpInput: CompanySignUp): Company
  postJob(jobInput: JobPosting): Job
  updateCompanyProfile(companyInput: CompanyInput): Company!
  updateStudentProfile(studentInput: StudentInput): Student!
  applyToJob(applyJobInput: JobApplication): Application
}

schema {
  query: RootQuery
  mutation: RootMutation
}
`);