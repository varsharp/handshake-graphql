const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');
var Schema = mongoose.Schema;

const EventSchema = new Schema({
    company: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'company'
    },
    name: {
        type: String,
    },
    description: {
        type: String,
    },
    date: {
        type: Date,
    },
    time: {
        type: String,
    },
    eligibility: {
        type: String,
    },
    city: {
        type: String,
    },
    state: {
        type: String,
    },
    country: {
        type: String,
    },
    registration: [
        {
            status: {
                type: String,
            },
            student: {
                type: mongoose.Schema.Types.ObjectId,
                ref: 'student'
            }            
        }
    ]
});

EventSchema.plugin(mongoosePaginate);

module.exports = Events = mongoose.model('event', EventSchema, 'event');
