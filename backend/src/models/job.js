const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');
var Schema = mongoose.Schema;

const JobSchema = new Schema({
    title: {
        type: String,
    },
    posting_date: {
        type: Date,
    },
    app_deadline: {
        type: Date,
    },
    location: {
        type: String,
        required: true,
    },
    salary: {
        type: Number,
    },
    description: {
        type: String,
    },
    job_type: {
        type: String,
        required: true,
    },
    company: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'company'
    },
    application: [
        {
            resume: {
                type: String,
            },
            status: {
                type: String,
                defaultValue: 'Pending',
            },
            applied_date: {
                type: Date,
            },
            student: {
                type: mongoose.Schema.Types.ObjectId,
                ref: 'student'
            }
        }
    ]
});

JobSchema.plugin(mongoosePaginate);

module.exports = Job = mongoose.model('job', JobSchema, 'job');
