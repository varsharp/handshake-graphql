export const ADD_EMAIL = 'ADD_EMAIL';
export const ADD_PASSWORD = 'ADD_PASSWORD';
export const ADD_SIGNUP_EMAIL = 'ADD_EMAIL';
export const ADD_SIGNUP_PASSWORD = 'ADD_PASSWORD';
export const ADD_FIRST_NAME = 'ADD_FIRST_NAME';
export const ADD_LAST_NAME = 'ADD_LAST_NAME';
export const ADD_COLLEGE_NAME = 'ADD_COLLEGE_NAME';
export const SAVE_BASIC_DETAILS = 'SAVE_BASIC_DETAILS';
export const SAVE_EDUCATION_INFO = 'SAVE_EDUCATION_INFO';
export const SAVE_EXPERIENCE_INFO = 'SAVE_EXPERIENCE_INFO';
export const SAVE_SKILLSET = 'SAVE_SKILLSET';
export const AUTH_SUCCESS = 'AUTH_SUCCESS';
export const AUTH_FAIL = 'AUTH_FAIL';
export const AUTH_LOGOUT = 'AUTH_LOGOUT';
export const SET_SIGNUP_ERROR = 'SET_SIGNUP_ERROR';
export const CHANGE_MODE = 'CHANGE_MODE';
export const ENABLE_SAVE = 'ENABLE_SAVE';
export const SAVE_PROFILE_PIC = 'SAVE_PROFILE_PIC';
export const CHANGE_EDUCATION_MODE = 'CHANGE_EDUCATION_MODE';
export const CHANGE_EXPERIENCE_MODE = 'CHANGE_EXPERIENCE_MODE';
export const SAVE_JOBS = 'SAVE_JOBS';
export const SAVE_STUDENTS = 'SAVE_STUDENTS';
export const RETURN_JOBS = 'RETURN_JOBS';
export const CONTROL_MODAL = 'CONTROL_MODAL';
export const SAVE_RESUME = 'SAVE_RESUME';
export const APPLY_TO_JOB = 'APPLY_TO_JOB';
export const SAVE_EVENTS = 'SAVE_EVENTS';
export const RETURN_EVENTS = 'RETURN_EVENTS';
export const CONTROL_EVENT_MODAL = 'CONTROL_EVENT_MODAL';
export const REGISTER_SUCCESS = 'REGISTER_SUCCESS';
export const REGISTER_FAILURE = 'REGISTER_FAILURE';
export const GET_JOBS = 'GET_JOBS';
export const RETURN_APPLIED_JOBS = 'RETURN_APPLIED_JOBS';
export const GET_ALL_JOBS = 'GET_ALL_JOBS';
export const ADD_TITLE = 'ADD_TITLE';
export const ADD_POSTING = 'ADD_POSTING_DATE';
export const ADD_DEADLINE = 'ADD_APP_DEADLINE';
export const ADD_LOCATION = 'ADD_LOCATION';
export const ADD_SALARY = 'ADD_SALARY';
export const ADD_DESCRIPTION = 'ADD_DESCRIPTION';
export const ADD_JOB_TYPE = 'ADD_JOB_TYPE';
export const CREATE_SUCCESS = 'CREATE_SUCCESS';
export const ADD_NAME = 'CREATE_SUCCESS';
export const ADD_EVENT_DESC = 'ADD_EVENT_DESC';
export const ADD_EVENT_DATE = 'ADD_EVENT_DATE';
export const ADD_EVENT_TIME = 'ADD_EVENT_TIME';
export const ADD_ELIGIBILITY = 'ADD_ELIGIBILITY';
export const ADD_EVENT_LOCATION = 'ADD_EVENT_LOCATION';
export const ADD_CITY = 'ADD_CITY';
export const ADD_STATE = 'ADD_STATE';
export const ADD_COUNTRY = 'ADD_COUNTRY';
export const ADD_COMPANY_NAME = 'ADD_COMPANY_NAME';
export const ADD_COMPANY_EMAIL = 'ADD_COMPANY_EMAIL';
export const ADD_COMPANY_PASSWORD = 'ADD_COMPANY_PASSWORD';
export const SET_COMPANY_ERROR = 'SET_COMPANY_ERROR';
export const ADD_COMPANY_LOGIN_EMAIL = 'ADD_COMPANY_LOGIN_EMAIL';
export const ADD_COMPANY_LOGIN_PASSWORD = 'ADD_COMPANY_LOGIN_PASSWORD';
export const AUTH_COMPANY_SUCCESS = 'AUTH_COMPANY_SUCCESS';
export const AUTH_COMPANY_FAIL = 'AUTH_COMPANY_FAIL';
export const SET_CURRENT_JOB_PAGE = 'SET_CURRENT_JOB_PAGE';
export const SET_SORT_BY_VALUE = 'SET_SORT_BY_VALUE';
export const SET_SORT_BY_ORDER = 'SET_SORT_BY_ORDER';   
export const SET_CURRENT_EVENT_PAGE = 'SET_CURRENT_EVENT_PAGE';
export const SET_CURRENT_APPLICATION_PAGE = 'SET_CURRENT_APPLICATION_PAGE';
export const SAVE_APPLICATIONS = 'SAVE_APPLICATIONS';
export const SET_CURRENT_COMPANY_JOB_PAGE = 'SET_CURRENT_COMPANY_JOB_PAGE';
export const UPDATE_APPLICATION_STATUS = 'UPDATE_APPLICATION_STATUS';
export const SAVE_COMPANY_EVENT_STUDENTS = 'SAVE_COMPANY_EVENT_STUDENTS';
export const SET_CURRENT_COMPANY_EVENT_PAGE = 'SET_CURRENT_COMPANY_EVENT_PAGE';
export const SAVE_COMPANY_EVENTS = 'SAVE_COMPANY_EVENTS';
export const SET_STUDENT_JOB_SEARCH_STR = 'SET_STUDENT_JOB_SEARCH_STR';
export const SET_STUDENT_JOB_SEARCH_VAL = 'SET_STUDENT_JOB_SEARCH_VAL';
export const SET_STUDENT_EVENT_SEARCH_VAL = 'SET_STUDENT_EVENT_SEARCH_VAL';
export const CHANGE_EDUCATION_KEY = 'CHANGE_EDUCATION_KEY';
export const CHANGE_EXPERIENCE_KEY = 'CHANGE_EXPERIENCE_KEY';
export const SAVE_STUDENT_LIST = 'SAVE_STUDENT_LIST';
export const RETURN_STUDENTS = 'RETURN_STUDENTS';
export const SET_CURRENT_STUDENT_PAGE = 'SET_CURRENT_STUDENT_PAGE';
export const SET_STUDENT_STUDENT_SEARCH_STR = 'SET_STUDENT_STUDENT_SEARCH_STR';
export const SET_STUDENT_STUDENT_SEARCH_VAL = 'SET_STUDENT_STUDENT_SEARCH_VAL';
export const CONTROL_MESSAGE_MODAL = 'CONTROL_MESSAGE_MODAL';
export const SEND_MESSAGE = 'SEND_MESSAGE';
export const SAVE_MESSAGE = 'SAVE_MESSAGE';
export const SAVE_MESSAGES = 'SAVE_MESSAGES';
export const SAVE_HISTORY = 'SAVE_HISTORY';
export const SAVE_REPLY = 'SAVE_REPLY';
export const SAVE_COMPANY_INFO = 'SAVE_COMPANY_INFO';
export const SAVE_CONTACT_INFO = 'SAVE_CONTACT_INFO';
export const SAVE_COMPANY_PROFILE_PIC = 'SAVE_COMPANY_PROFILE_PIC';
export const CHANGE_COMPANY_MODE = 'CHANGE_COMPANY_MODE';
export const ENABLE_COMPANY_SAVE = 'ENABLE_COMPANY_SAVE';
