import React, { Component } from 'react';
import { Applications } from '../../components/application/application';
import { ApplicationSearch } from '../../components/application-search/appSearch';
import { Container } from 'react-bootstrap';
import axios from 'axios';
import { PATH } from '../../config';
import { connect } from 'react-redux';
import { saveApplications, setCurrentApplicationPage, returnAppliedJobs } from './store/action';

class Application extends Component {

    filters = [];

    componentDidMount() {
        this.getApplications(this.props.currentPage);
    }

    getApplications = (page) => {
        let params = new URLSearchParams();
        params.set('page', page);
        if(this.filters.length) {
            params.set('filter', this.filters[0])
        }

        axios.defaults.headers.common['authorization'] = localStorage.getItem('token');
        axios.get(PATH  + "/jobs/applications?" + params.toString())
        .then(res => {
            if(res.status === 200){
                if(res.data.applications.responseMessage){
                    this.props.saveApplications(res.data.applications.responseMessage);                 
                }
            }
        })
        .catch(err=>{
            //this.props.setError(err.response.data);
        })
    }

    // search = (event) => {
    //     event.preventDefault();
    //     let jobs = [];
    //     jobs = this.props.jobs.filter(job => {
    //         return job['status'] === event.target.innerText;
    //     });
    //     this.props.returnAppliedJobs(jobs);
    // }

    recordFilters = (event) => {
        this.filters.push(event.target.innerText);
        this.getApplications(this.props.currentPage);
    }

    pageChanged = (e) => {
        this.props.setCurrentApplicationPage(Number(e.target.text));
        this.getApplications(e.target.text);
    };

    render() {
        return (            
            <Container className="mt-5 mb-5">
                <h1 class="display-4">View Applications</h1>
                <div className="w-100 bg-light text-dark p-5 shadow rounded">
                    <ApplicationSearch recordFilters={this.recordFilters}></ApplicationSearch>
                </div>
                <div className="w-100 bg-light text-dark mt-5 p-5 shadow rounded">
                    <Applications applications = { this.props.applications } searchResults = { this.props.searchResults } currentPage = { this.props.currentPage } pageChanged = { this.pageChanged }></Applications>
                </div>
            </Container>  
        )
    };
};

const mapStateToProps = (state) => {
    return {
        applications: state.app.applications,
        currentPage: state.app.currentPage,
        searchResults: state.app.searchResults,
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        saveApplications: (data) => dispatch(saveApplications(data)),
        returnAppliedJobs: (data) => dispatch(returnAppliedJobs(data)),
        setCurrentApplicationPage: (data) => dispatch(setCurrentApplicationPage(data))
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(Application);