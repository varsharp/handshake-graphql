import * as actionTypes from '../../../constants/action-types';

export const saveApplications = (payload) => {
    return { type: actionTypes.SAVE_APPLICATIONS, payload }
};

export const setCurrentApplicationPage = (payload) => {
    return { type: actionTypes.SET_CURRENT_APPLICATION_PAGE, payload }
};

export const returnAppliedJobs = (payload) => {
    return { type: actionTypes.RETURN_APPLIED_JOBS, payload }
};