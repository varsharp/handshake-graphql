import React, { Component } from 'react';
import { JobSearch } from '../../components/job-search/jobSearch';
import { Jobs } from '../../components/jobs/jobs';
import { Container } from 'react-bootstrap';
import axios from 'axios';
import { PATH } from '../../config';
import { connect } from 'react-redux';
import { saveJobs, returnJobs, setCurrentJobPage, setSearchStr, setSearchVal, setSortByValue, setSortByOrder, controlModal, saveResume, applyToJob } from './store/action';

class Dashboard extends Component {

    filters = [];
    selectedJob = {};
    jobIdApplied = null;

    componentDidMount() {
        this.getJobs(this.props.currentPage, '', null);
    }

    getJobs = (page, sortValue, sortOrder, searchStr, searchVal) => {
        if(sortOrder == 0) sortOrder = -1;
        if(sortOrder == 1) sortOrder = 1;
        if(sortValue === 'application deadline') sortValue = 'app_deadline';
        if(sortValue === 'posting date') sortValue = 'posting_date';
        let params = new URLSearchParams();

        if(searchStr) {
            params.set('searchStr', searchStr);
        }
        if(searchVal) {
            params.set('searchVal', searchVal);
        }
                
        params.set('page', page);
        params.set('sortBy', sortValue);
        params.set('order', sortOrder);

        if(this.filters.length) {
            params.set('filter', this.filters[0])
        }
        axios.defaults.headers.common['authorization'] = localStorage.getItem('token');
        axios.get(PATH  + "/jobs?" + params.toString())
        .then(res => {
            if(res.status === 200){
                if(res.data){
                    this.props.saveJobs(res.data.jobs.responseMessage);
                }
            }
        })
        .catch(err=>{
            //this.props.setError(err.response.data);
        })
    }

    search = (event) => {
        event.preventDefault();
        this.props.setSearchStr(event.target.elements[0].getAttribute('id'));
        this.props.setSearchVal(event.target.elements[0].value.toLowerCase());
        this.getJobs(1, this.props.sortByValue, this.props.sortByOrder, event.target.elements[0].getAttribute('id'), event.target.elements[0].value.toLowerCase());
    }

    recordFilters = (event) => {
        this.filters.push(event.target.innerText);
    }

    controlModal = (action, job) => {
        this.props.controlModal(action);
        this.selectedJob = job;
    }

    saveResume = (event, jobId) => {        
        event.preventDefault();
        const formData = new FormData();
        const file = event.target.form.elements[0].files[0];
        formData.append('resume', file);
        formData.append('id', jobId);
        axios.post(PATH + "/jobs/application", formData, {
            headers: {
                'content-type':'multipart/form-data'
            }
        })
        .then(res => {
            if(res.status === 200){
                this.jobIdApplied = jobId;
                this.props.saveResume(PATH + "/" + file.name);
            }
        })
        .catch(err=>{
            //this.props.authFail(err.response.data.msg);
        })        
    }

    applyToJob = () => {
        this.props.applyToJob(true);
    }

    pageChanged = (e) => {
        this.props.setCurrentJobPage(Number(e.target.text));
        this.getJobs(e.target.text, this.props.sortByValue, this.props.sortByOrder, this.props.searchStr, this.props.searchVal);
    };

    setSortByOrder = () => {
        this.props.setSortByOrder(!this.props.sortByOrder);
        this.getJobs(this.props.currentPage, this.props.sortByValue, !this.props.sortByOrder, this.props.searchStr, this.props.searchVal);
    }

    setSortByValue = (e) => {
        this.props.setSortByValue(e.target.innerText.toLowerCase());
        this.getJobs(this.props.currentPage, e.target.innerText.toLowerCase(), this.props.sortByOrder, this.props.searchStr, this.props.searchVal);
    }

    viewProfile = (job) => {
        let id = job.company;
        this.props.history.push({
            pathname: '/companyprofile',
            search: '?view=' + id
        });
    }

    render() {
        return (
            <Container className="mt-5 mb-5">
                <h1 className="display-4">Job Search</h1>
                <div className="w-100 bg-light text-dark p-5 shadow rounded">
                    <JobSearch submitHandler={ this.search } recordFilters = { this.recordFilters } setSortByValue = { this.setSortByValue } setSortByOrder = { this.setSortByOrder }></JobSearch>
                </div>
                <div className="w-100 bg-light text-dark mt-5 p-5 shadow rounded">
                    <Jobs jobs = { this.props.jobs } viewProfile = {this.viewProfile} searchResults = { this.props.searchResults } controlModal = { this.controlModal } openModal = {this.props.openModal} selectedJob = {this.selectedJob} applyToJob = {this.applyToJob} success = {this.props.success} saveResume = {this.saveResume} jobIdApplied = {this.jobIdApplied} pageChanged = { this.pageChanged } currentPage = { this.props.currentPage }></Jobs>
                </div>
            </Container>
        )
    };
};

const mapStateToProps = (state) => {
    return {
        jobs: state.job.jobs,
        currentPage: state.job.currentPage,
        searchStr: state.job.searchStr,
        searchVal: state.job.searchVal,
        sortByValue: state.job.sortByValue,
        sortByOrder: state.job.sortByOrder,
        searchResults: state.job.searchResults,
        openModal: state.job.openModal,
        success: state.job.success
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        saveJobs: (data) => dispatch(saveJobs(data)),
        returnJobs: (data) => dispatch(returnJobs(data)),
        setCurrentJobPage: (data) => dispatch(setCurrentJobPage(data)),
        setSearchStr: (data) => dispatch(setSearchStr(data)),
        setSearchVal: (data) => dispatch(setSearchVal(data)),
        setSortByValue: (data) => dispatch(setSortByValue(data)),
        setSortByOrder: (data) => dispatch(setSortByOrder(data)),
        controlModal: (data) => dispatch(controlModal(data)),
        saveResume: (data) => dispatch(saveResume(data)),
        applyToJob: (data) => dispatch(applyToJob(data)),
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);