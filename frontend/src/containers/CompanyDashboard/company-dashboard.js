import React, { Component } from 'react';
import { Container, Button } from 'react-bootstrap';
import axios from 'axios';
import { PATH } from '../../config';
import { connect } from 'react-redux';
import { saveJobs, saveStudents, updateStatus, setCurrentCompanyJobPage } from './store/action';
import { CreateData } from '../../components/create-data/create-data';

class CompanyDashboard extends Component {
    studentId = null;

    componentDidMount() {
        this.getJobs(this.props.currentPage);
    }

    getJobs = (page) => {
        axios.defaults.headers.common['authorization'] = localStorage.getItem('token');
        axios.get(PATH  + "/job/all?page=" + page)
        .then(res => {
            if(res.status === 200){
                if(res.data && res.data.jobs.responseMessage){
                    this.props.saveJobs(res.data.jobs.responseMessage);
                }
            }
        })
        .catch(err=>{
            //this.props.setError(err.response.data);
        })
    }

    getStudents = (jobId) => {
        axios.defaults.headers.common['authorization'] = localStorage.getItem('token');
        axios.get(PATH  + "/job/students?jobId=" + jobId)
        .then(res => {
            if(res.status === 200){
                if(res.data && res.data.students.responseMessage){
                    this.props.saveStudents(res.data.students.responseMessage);
                } else {
                    this.displayMsg = "This job has no applicants.";
                }
            }
        })
        .catch(err=>{
            //this.props.setError(err.response.data);
        })
    }

    updateStatus = (event, studentId) => {
        this.studentId = studentId;
        this.props.updateStatus(event.target.innerText);
    }

    pageChanged = (e) => {
        this.props.setCurrentCompanyJobPage(Number(e.target.text));
        this.getJobs(e.target.text);
    };

    viewProfile = (student) => {
        let id = student._id;
        this.props.history.push({
            pathname: '/profile',
            search: '?view=' + id
        });
    }

    render() {
        return (            
            <Container className="mt-5 mb-5">
                <Button type = "button" onClick = { () => this.props.history.push('/createjob') }>Create Job</Button>
                { this.props.jobs && this.props.jobs.length && 
                <div className="w-100 bg-light text-dark mt-5 p-5 shadow rounded">
                    <CreateData jobs = { this.props.jobs } currentPage = { this.props.currentPage } pageChanged = { this.pageChanged } getStudents = { this.getStudents } students = { this.props.students } updateStatus = { this.updateStatus } status = { this.props.status } studentId = {this.studentId} viewProfile = {this.viewProfile} ></CreateData>
                </div> }
            </Container>
        )
    };
};

const mapStateToProps = (state) => {
    return {
        jobs: state.companyJob.jobs,
        students: state.companyJob.students,
        status: state.companyJob.status,
        currentPage: state.companyJob.currentPage,
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        saveJobs: (data) => dispatch(saveJobs(data)),
        saveStudents: (data) => dispatch(saveStudents(data)),
        updateStatus: (data) => dispatch(updateStatus(data)),
        setCurrentCompanyJobPage: (data) => dispatch(setCurrentCompanyJobPage(data)),
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(CompanyDashboard);