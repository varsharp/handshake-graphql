import * as actionTypes from '../../../constants/action-types';

export const saveJobs = (payload) => {
    return { type: actionTypes.SAVE_JOBS, payload}
};

export const saveStudents = (payload) => {
    return { type: actionTypes.SAVE_STUDENTS, payload}
};

export const updateStatus = (payload) => {
    return { type: actionTypes.UPDATE_APPLICATION_STATUS, payload}
};

// export const saveResume = (payload) => {
//     return { type: actionTypes.SAVE_RESUME, payload }
// };

export const setCurrentCompanyJobPage = (payload) => {
    return { type: actionTypes.SET_CURRENT_COMPANY_JOB_PAGE, payload }
};