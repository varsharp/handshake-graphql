import * as actionTypes from '../../../constants/action-types';

export const saveCompanyInfo = (payload) => {
    return { type: actionTypes.SAVE_COMPANY_INFO, payload}
};

export const saveContactInfo = (payload) => {
    return { type: actionTypes.SAVE_CONTACT_INFO, payload}
};

export const saveCompanyProfilePic = (payload) => {
    return { type: actionTypes.SAVE_COMPANY_PROFILE_PIC, payload }
};

export const changeMode = (payload) => {
    return { type: actionTypes.CHANGE_COMPANY_MODE, payload }
};

export const enableSave = (payload) => {
    return { type: actionTypes.ENABLE_COMPANY_SAVE, payload }
};
