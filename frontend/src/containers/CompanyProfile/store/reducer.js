import * as actionTypes from '../../../constants/action-types';
import user from '../../../assets/user.png';

const initialState = {
    companyInfo: null,
    description: null,
    contactInfo: null,
    mode: false,
    contactmode: false,
    save: false,
    profile_pic: user,
    success: false,
}

const companyProfileReducer = (state = initialState, action) => {
    switch(action.type){
        case actionTypes.SAVE_COMPANY_INFO:
            return {
                ...state,
                companyInfo: action.payload,                
            }
        case actionTypes.SAVE_CONTACT_INFO:
            return {
                ...state,
                experience: action.payload,
            }
        case actionTypes.CHANGE_COMPANY_MODE:
            return {
                ...state,
                mode: action.payload
            }     
        case actionTypes.ENABLE_COMPANY_SAVE:
            return {
                ...state,
                save: action.payload
            }
        case actionTypes.SAVE_COMPANY_PROFILE_PIC:
            return {
                ...state,
                profile_pic: action.payload
            }
        default:
            return initialState;
    }
}

export default companyProfileReducer;