import React, { Component } from 'react';
import { CreateData } from '../../components/create-data/create-data';
import { Container, Button } from 'react-bootstrap';
import axios from 'axios';
import { PATH } from '../../config';
import { connect } from 'react-redux';
import { saveEvents, saveStudents, setCurrentCompanyEventPage } from './store/action';

class CompanyEvents extends Component {

    componentDidMount() {
        this.getEvents(this.props.currentPage);
    }

    getEvents = (page) => {
        axios.defaults.headers.common['x-auth-token'] = localStorage.getItem('token');
        axios.get(PATH  + "/event/all?page=" + page)
        .then(res => {
            if(res.status === 200){
                if(res.data){
                    this.props.saveEvents(res.data);
                }
            }
        })
        .catch(err=>{
            //this.props.setError(err.response.data);
        })
    }

    getStudents = (eventId) => {
        axios.defaults.headers.common['x-auth-token'] = localStorage.getItem('token');
        axios.get(PATH  + "/event/students?eventId=" + eventId)
        .then(res => {
            if(res.status === 200){
                if(res.data && res.data.length){
                    this.props.saveStudents(res.data);
                } else {
                    this.displayMsg = "This event has no registrations.";
                }
            }
        })
        .catch(err=>{
            //this.props.setError(err.response.data);
        })
    }

    pageChanged = (e) => {
        this.props.setCurrentCompanyEventPage(Number(e.target.text));
        this.getEvents(e.target.text);
    };

    render() {
        return (            
            <Container className="mt-5 mb-5">
                <Button type = "button" onClick = { () => this.props.history.push('/createevent') }>Create Event</Button>
                { this.props.events && this.props.events.length && 
                <div className="w-100 bg-light text-dark mt-5 p-5 shadow rounded">
                    <CreateData events = { this.props.events } currentPage = { this.props.currentPage } pageChanged = { this.pageChanged } getStudents = { this.getStudents } students = { this.props.students } ></CreateData>
                </div> }
            </Container>  
        )
    };
};

const mapStateToProps = (state) => {
    return {
        events: state.companyEvent.events,
        students: state.companyEvent.students,
        currentPage: state.companyEvent.currentPage,
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        saveEvents: (data) => dispatch(saveEvents(data)),
        saveStudents: (data) => dispatch(saveStudents(data)),
        setCurrentCompanyEventPage: (data) => dispatch(setCurrentCompanyEventPage(data)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CompanyEvents);