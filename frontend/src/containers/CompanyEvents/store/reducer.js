import * as actionTypes from '../../../constants/action-types';

const initialState = {
    events: null,
    students: null,
    currentPage: 1
}
 
const companyEventReducer = (state = initialState, action) => {
    switch(action.type){
        case actionTypes.SAVE_COMPANY_EVENTS:
            return {
                ...state,
                events: action.payload,                
            }
        case actionTypes.SAVE_COMPANY_EVENT_STUDENTS:
            return {
                ...state,
                students: action.payload,                
            }
        case actionTypes.SET_CURRENT_COMPANY_EVENT_PAGE:
            return {
                ...state,
                currentPage: action.payload,                
            }
        default:
            return initialState;
    }
}

export default companyEventReducer;