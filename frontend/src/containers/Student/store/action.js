import * as actionTypes from '../../../constants/action-types';

export const saveStudents = (payload) => {
    return { type: actionTypes.SAVE_STUDENT_LIST, payload}
};

export const returnStudents = (payload) => {
    return { type: actionTypes.RETURN_STUDENTS, payload}
};

export const setCurrentStudentPage = (payload) => {
    return { type: actionTypes.SET_CURRENT_STUDENT_PAGE, payload }
};

export const setSearchStr = (payload) => {
    return { type: actionTypes.SET_STUDENT_STUDENT_SEARCH_STR, payload }
};

export const setSearchVal = (payload) => {
    return { type: actionTypes.SET_STUDENT_STUDENT_SEARCH_VAL, payload }
};
