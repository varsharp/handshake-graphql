import * as actionTypes from '../../../constants/action-types';
import user from '../../../assets/user.png';

const initialState = {
    basicDetails: null,
    education: [],
    experience: null,
    skillset: [],
    mode: false,
    save: false,
    profile_pic: user,
    edMode: false,
    edKey: null,
    expKey: null,
    expMode: false,
    showMessageModal: false,
    success: false,
    message: null
}
 
const profileReducer = (state = initialState, action) => {
    switch(action.type){
        case actionTypes.SAVE_BASIC_DETAILS:
            return {
                ...state,
                basicDetails: action.payload,                
            }
        case actionTypes.SAVE_EDUCATION_INFO:
            return {
                ...state,
                education: action.payload,
            }
        case actionTypes.SAVE_EXPERIENCE_INFO:
            return {
                ...state,
                experience: action.payload,
            }
        case actionTypes.SAVE_SKILLSET:
            return {
                ...state,
                skillset: action.payload
            }
        case actionTypes.CHANGE_MODE:
            return {
                ...state,
                mode: action.payload
            }
        case actionTypes.CHANGE_EDUCATION_MODE:
            return {
                ...state,
                edMode: action.payload
            }
        case actionTypes.CHANGE_EDUCATION_KEY:
            return {
                ...state,
                edKey: action.payload
            }
        case actionTypes.CHANGE_EXPERIENCE_KEY:
            return {
                ...state,
                expKey: action.payload
            }
        case actionTypes.CHANGE_EXPERIENCE_MODE:
            return {
                ...state,
                expMode: action.payload
            }
        case actionTypes.ENABLE_SAVE:
            return {
                ...state,
                save: action.payload
            }
        case actionTypes.SAVE_PROFILE_PIC:
            return {
                ...state,
                profile_pic: action.payload
            }
        case actionTypes.CONTROL_MESSAGE_MODAL:
            return {
                ...state,
                showMessageModal: action.payload,                
            }
        case actionTypes.SEND_MESSAGE:
            return {
                ...state,
                success: action.payload,                
            }
        case actionTypes.SAVE_MESSAGE:
            return {
                ...state,
                message: action.payload,                
            }
        default:
            return initialState;
    }
}

export default profileReducer;