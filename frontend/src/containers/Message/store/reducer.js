import * as actionTypes from '../../../constants/action-types';

const initialState = {
    messages: [],
    history: [],
    reply: null
}
 
const msgReducer = (state = initialState, action) => {
    switch(action.type){
        case actionTypes.SAVE_MESSAGES:
            return {
                ...state,
                messages: action.payload,                
            }
        case actionTypes.SAVE_HISTORY:
            return {
                ...state,
                history: action.payload,
            }
        case actionTypes.SAVE_REPLY:
            return {
                ...state,
                reply: action.payload,                
            }
        default:
            return initialState;
    }
}

export default msgReducer;