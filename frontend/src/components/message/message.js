import React from 'react';
import { Button, Card, Badge, Row, Col, Form } from 'react-bootstrap';

export const MessageComponent = (props) => {
    let messageList = [];
    if(props.messages) {
        let messages = props.messages;
        messageList = Object.keys(messages).map(key =>
            <Card bg="light" className = "mt-2">
                <Card.Body>
                <Button type="button" variant="link" className="p-0" onClick={ () => props.getHistory(messages[key]) }><Badge variant="info">{messages[key].entity}</Badge></Button>
                <Card.Text id="date">
                    {messages[key].date}
                </Card.Text>
                <Card.Text id="email">
                    {messages[key].email}
                </Card.Text>          
                </Card.Body>
            </Card>
        );
    } 
    // else if(props.events) {
    //     let events = props.events;
    //     datalist = Object.keys(events).map(key =>
    //         <Card bg="light" className = "mt-2">
    //             <Card.Body>
    //             <Button type="button" variant="link" className="p-0" onClick={ () => props.getStudents(events[key]._id) }>{events[key].name}</Button>
    //             <Card.Text id="desc">
    //                 {events[key].description}
    //             </Card.Text>
    //             <Card.Text id="location">
    //                 {events[key].city}, {events[key].state}, {events[key].country}
    //             </Card.Text>
    //             <Card.Text id="eligibility">
    //                 {events[key].eligibility} majors
    //             </Card.Text>
    //             <Card.Text id="posting_date">
    //                 {events[key].date}
    //             </Card.Text>          
    //             </Card.Body>
    //         </Card>
    //     );
    // }
    
    let historyList = [];
    if(props.history) {
        let history = props.history;
        historyList = Object.keys(history).map(key =>
            <Card bg="light" className = "mt-2">
                <Card.Body>
                {history[key].action === 'Received' && <Badge variant="info">{props.msg.entity}</Badge>}
                {history[key].action === 'Sent' && <Badge variant="secondary">Me</Badge>}
                <Card.Text id="date">
                    {history[key].date}
                </Card.Text>
                <Card.Text id="body">
                    {history[key].body}
                </Card.Text>
                </Card.Body>
            </Card>
        );
    }

    return (
        <div>
            <Row>
                <Col lg={5}>{messageList}</Col>
                <Col lg={7}>
                    {historyList}
                    {historyList.length > 0 && <Form className="mt-2" id="message-form">
                        <Form.Group>
                            <Form.Control as="textarea" placeholder = "Type a message..." rows="3" onChange={props.updateMsgHandler} />
                        </Form.Group>
                        <Button variant="primary" type="submit" onClick={props.sendMessage}>
                            Send
                        </Button>
                    </Form>}
                </Col>          
            </Row>
        </div>      
    );
}