import React from 'react';
import { Card, Form, Button, Row, Col } from 'react-bootstrap';
import { faEdit } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export const CompanyDescription = (props) => {  
    let content;

    if(props.save){
        content = (
            <div>
                <Form onSubmit={props.submitHandler}>
                <Form.Group>
                    <Form.Control as="textarea" rows="3" defaultValue={ props.description } onChange={props.updateHandler} />
                </Form.Group>
                <Button variant="success" type="submit">
                    Save
                </Button>
                <Button variant="danger" type="button" className = 'ml-2' onClick = { () => props.enableSave(false) }>
                    Cancel
                </Button>
                </Form>
            </div>
        )
    } else {
        content = (
            <Row>
                <Col>
                <Form.Control plaintext defaultValue={ props.description } rows="3" readOnly />
                </Col>
            </Row>
        )
    }

    return (
        <Card bg="light">
            <Card.Body>
            {!props.viewId && <Row><Button variant="link" style={{paddingLeft: '680px'}} onClick={props.enableSave}><FontAwesomeIcon icon={faEdit} /></Button></Row>}
            <Card.Title>Description</Card.Title>
            {content}        
            </Card.Body>
        </Card>
    );  
}