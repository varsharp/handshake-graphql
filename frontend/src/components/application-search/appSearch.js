import React from 'react';
import { Form, Col } from 'react-bootstrap';

export const ApplicationSearch = (props) => {
    return (
        <Form>
            <Form.Row className='w-50'>
                <Form.Group as={Col} md="3" controlId="pendingFilter">
                    <button type="button" onClick = {props.recordFilters} className="btn btn-outline-primary">Pending</button>
                </Form.Group>
                <Form.Group as={Col} md="3" controlId="reviewedFilter">
                    <button type="button" onClick = {props.recordFilters} className="btn btn-outline-primary">Reviewed</button>
                </Form.Group>
                <Form.Group as={Col} md="3" controlId="declinedFilter">
                    <button type="button" onClick = {props.recordFilters} className="btn btn-outline-primary">Declined</button>
                </Form.Group>
            </Form.Row>
        </Form>
    );
}