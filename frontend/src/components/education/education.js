import React from 'react';
import { Card, Form, Col, Button } from 'react-bootstrap';
import { faEdit, faTrashAlt } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export const Education = (props) => {
    let content;
    
    if(!props.edMode && props.education && props.education.length){
        content = Object.keys(props.education).map(key =>
            <div className = "shadow p-3 mb-5 bg-white rounded">
                {!props.viewId && <Button variant="link" className = "position-relative float-right" onClick={() => props.changeEdMode(true, key)}><FontAwesomeIcon icon={faEdit} /></Button>}
                <Card.Text>
                    College name: {(props.education && props.education.length) ? props.education[key].college_name : ''}
                </Card.Text>
                <Card.Text>
                    Degree: {(props.education && props.education.length) ? props.education[key].degree : ''}
                </Card.Text>
                <Card.Text>
                    Year of Passing: {(props.education && props.education.length) ? props.education[key].year_of_passing: ''}
                </Card.Text>
                <Card.Text>
                    Major: {(props.education && props.education.length) ? props.education[key].major: ''}
                </Card.Text>
                <Card.Text>
                    GPA: {(props.education && props.education.length) ? props.education[key].cgpa: ''}
                </Card.Text>
                {!props.viewId && <Button variant="link" className = "position-relative float-right" style={{bottom: '30px'}} onClick={() => props.deleteEducation(key, props.education[key]._id)}><FontAwesomeIcon icon={faTrashAlt} /></Button>}
            </div>
        );
    } else if(props.edKey) {
        content = (
            <Form onSubmit = {props.editEducationInfo}>
                <Form.Row>
                    <Form.Group as={Col} controlId="formGridCollege">
                    <Form.Label>College Name</Form.Label>
                    <Form.Control type="text" defaultValue={(props.education && props.education.length) ? props.education[props.edKey].college_name : ''} placeholder="Enter college" />
                    </Form.Group>

                    <Form.Group as={Col} controlId="formGridDegree">
                    <Form.Label>Degree</Form.Label>
                    <Form.Control as="select" defaultValue={(props.education && props.education.length) ? props.education[props.edKey].degree : ''}>
                        <option>Choose...</option>
                        <option>High School</option>
                        <option>Associates</option>
                        <option>Certificate</option>
                        <option>Advanced Certificate</option>
                        <option>Bachelors</option>
                        <option>Masters</option>
                        <option>Doctorate</option>
                        <option>Postdoctoral Studies</option>
                        <option>Non-Degree seeking</option>
                    </Form.Control>
                    </Form.Group>
                </Form.Row>

                <Form.Group controlId="formGridYear">
                    <Form.Label>Year of Passing</Form.Label>
                    <Form.Control defaultValue = {(props.education && props.education.length) ? props.education[props.edKey].year_of_passing: ''} placeholder="Enter year of passing" />
                </Form.Group>

                <Form.Group controlId="formGridMajor">
                    <Form.Label>Major</Form.Label>
                    <Form.Control defaultValue = {(props.education && props.education.length) ? props.education[props.edKey].major: ''} placeholder="Enter major" />
                </Form.Group>

                <Form.Row>
                    <Form.Group as={Col} controlId="formGridCity">
                    <Form.Label>City</Form.Label>
                    <Form.Control defaultValue = {(props.education && props.education.length) ? props.education[props.edKey].city: ''} />
                    </Form.Group>

                    <Form.Group as={Col} controlId="formGridState">
                    <Form.Label>State</Form.Label>
                    <Form.Control type="text" defaultValue = {(props.education && props.education.length) ? props.education[props.edKey].state: ''} placeholder="Enter state" />
                    </Form.Group>

                    <Form.Group as={Col} controlId="formGridCountry">
                    <Form.Label>Country</Form.Label>
                    <Form.Control type="text" defaultValue = {(props.education && props.education.length) ? props.education[props.edKey].country: ''} placeholder="Enter Country" />
                    </Form.Group>
                </Form.Row>

                <Form.Group controlId="formGridGpa">
                    <Form.Label>GPA</Form.Label>
                    <Form.Control type="number" defaultValue = {(props.education && props.education.length) ? props.education[props.edKey].cgpa: ''} placeholder="Enter GPA" />
                </Form.Group>

                <Button variant="primary" type="submit">
                    Submit
                </Button>
            </Form>
        )
    } else {
        content = (
            <Form onSubmit = {props.submitHandler}>
                <Form.Row>
                    <Form.Group as={Col} controlId="formGridCollege">
                    <Form.Label>College Name</Form.Label>
                    <Form.Control type="text" placeholder="Enter college" />
                    </Form.Group>

                    <Form.Group as={Col} controlId="formGridDegree">
                    <Form.Label>Degree</Form.Label>
                    <Form.Control as="select" >
                        <option>Choose...</option>
                        <option>High School</option>
                        <option>Associates</option>
                        <option>Certificate</option>
                        <option>Advanced Certificate</option>
                        <option>Bachelors</option>
                        <option>Masters</option>
                        <option>Doctorate</option>
                        <option>Postdoctoral Studies</option>
                        <option>Non-Degree seeking</option>
                    </Form.Control>
                    </Form.Group>
                </Form.Row>

                <Form.Group controlId="formGridYear">
                    <Form.Label>Year of Passing</Form.Label>
                    <Form.Control placeholder="Enter year of passing" />
                </Form.Group>

                <Form.Group controlId="formGridMajor">
                    <Form.Label>Major</Form.Label>
                    <Form.Control placeholder="Enter major" />
                </Form.Group>

                <Form.Row>
                    <Form.Group as={Col} controlId="formGridCity">
                    <Form.Label>City</Form.Label>
                    <Form.Control />
                    </Form.Group>

                    <Form.Group as={Col} controlId="formGridState">
                    <Form.Label>State</Form.Label>
                    <Form.Control type="text" placeholder="Enter state" />
                    </Form.Group>

                    <Form.Group as={Col} controlId="formGridCountry">
                    <Form.Label>Country</Form.Label>
                    <Form.Control type="text" placeholder="Enter Country" />
                    </Form.Group>
                </Form.Row>

                <Form.Group controlId="formGridGpa">
                    <Form.Label>GPA</Form.Label>
                    <Form.Control type="number" placeholder="Enter GPA" />
                </Form.Group>

                <Button variant="primary" type="submit">
                    Submit
                </Button>
            </Form>
        )
    }

    return (
        <Card bg="light">
            <Card.Body>
            <Card.Title>Education</Card.Title>
            {content}            
            </Card.Body>
            <Card.Footer>
            {!props.viewId && <Button variant="link" onClick = { () => props.changeEdMode(true) }>Add School</Button>}
            </Card.Footer>
        </Card>
    );  
}