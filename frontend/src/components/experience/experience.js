import React from 'react';
import { Card, Form, Button, Col } from 'react-bootstrap';
import { faEdit, faTrashAlt } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export const Experience = (props) => {
    let content;
    
    if(!props.expMode && props.experience && props.experience.length){
        content = Object.keys(props.experience).map(key =>
            <div className = "shadow p-3 mb-5 bg-white rounded">
                {!props.viewId && <Button variant="link" className = "position-relative float-right" onClick={() => props.changeExpMode(true, key)}><FontAwesomeIcon icon={faEdit} /></Button>}
                <Card.Text>
                    Company: {(props.experience && props.experience.length) ? props.experience[key].company : ''}
                </Card.Text>
                <Card.Text>
                    Title: {(props.experience && props.experience.length) ? props.experience[key].title : ''}
                </Card.Text>
                <Card.Text>
                    Duration: {(props.experience && props.experience.length) ? props.experience[key].start_date + ' to' : ''} {(props.experience && props.experience.length) ? props.experience[key].end_date: ''}
                </Card.Text>
                <Card.Text>
                    Work Description: {(props.experience && props.experience.length) ? props.experience[key].description : ''}
                </Card.Text> 
                <Card.Text>
                    Location: {(props.experience && props.experience.length) ? props.experience[key].city : ''}, {(props.experience && props.experience.length) ? props.experience[key].country : ''}
                </Card.Text>
                {!props.viewId && <Button variant="link" className = "position-relative float-right" style={{bottom: '30px'}} onClick={() => props.deleteExperience(key, props.experience[key]._id)}><FontAwesomeIcon icon={faTrashAlt} /></Button>}         
            </div>
        );
    } else if(props.expKey){
        content = (
            <Form onSubmit = {props.editExperienceInfo}>
            <Form.Row>
            <Form.Group as={Col} controlId="formGridCompanyName">
                <Form.Label>Company Name</Form.Label>
                    <Form.Control type="text" defaultValue = {(props.experience && props.experience.length) ? props.experience[props.expKey].company : ''} placeholder="Enter company name" />
                </Form.Group>


                <Form.Group as={Col} controlId="formGridTitle">
                    <Form.Label>Title</Form.Label>
                    <Form.Control type="text" defaultValue = {(props.experience && props.experience.length) ? props.experience[props.expKey].title : ''} placeholder="Enter title" />
                </Form.Group>
            </Form.Row>

            <Form.Row>
            <Form.Group as={Col} controlId="formGridStartDate">
                <Form.Label>Start Date</Form.Label>
                    <Form.Control defaultValue = {(props.experience && props.experience.length) ? props.experience[props.expKey].start_date : ''} type="date"/>
                </Form.Group>

                <Form.Group as={Col} controlId="formGridEndDate">
                    <Form.Label>End Date</Form.Label>
                    <Form.Control defaultValue = {(props.experience && props.experience.length) ? props.experience[props.expKey].end_date : ''} type="date" />
                </Form.Group>
            </Form.Row>

            <Form.Row>
                <Form.Group as={Col} controlId="formGridCity">
                <Form.Label>City</Form.Label>
                <Form.Control defaultValue = {(props.experience && props.experience.length) ? props.experience[props.expKey].city : ''} placeholder = "Enter City"/>
                </Form.Group>

                <Form.Group as={Col} controlId="formGridState">
                <Form.Label>State</Form.Label>
                <Form.Control type="text" defaultValue = {(props.experience && props.experience.length) ? props.experience[props.expKey].state : ''} placeholder="Enter State" />
                </Form.Group>

                <Form.Group as={Col} controlId="formGridCountry">
                <Form.Label>Country</Form.Label>
                <Form.Control type="text" defaultValue = {(props.experience && props.experience.length) ? props.experience[props.expKey].country : ''} placeholder="Enter Country" />
                </Form.Group>
            </Form.Row>

            <Form.Group controlId="formGridDesc">
                <Form.Label>Work Description</Form.Label>
                <Form.Control as="textarea" defaultValue = {(props.experience && props.experience.length) ? props.experience[props.expKey].description : ''} rows="3" placeholder= "Describe roles and responsibilities" />
            </Form.Group>

            <Button variant="primary" type="submit">
                Submit
            </Button>
            </Form>
        )
    } else {
        content = (
            <Form onSubmit = {props.submitHandler}>
            <Form.Row>
            <Form.Group as={Col} controlId="formGridCompanyName">
                <Form.Label>Company Name</Form.Label>
                    <Form.Control type="text" placeholder="Enter company name" />
                </Form.Group>


                <Form.Group as={Col} controlId="formGridTitle">
                    <Form.Label>Title</Form.Label>
                    <Form.Control type="text" placeholder="Enter title" />
                </Form.Group>
            </Form.Row>

            <Form.Row>
            <Form.Group as={Col} controlId="formGridStartDate">
                <Form.Label>Start Date</Form.Label>
                    <Form.Control type="date"/>
                </Form.Group>

                <Form.Group as={Col} controlId="formGridEndDate">
                    <Form.Label>End Date</Form.Label>
                    <Form.Control type="date" />
                </Form.Group>
            </Form.Row>

            <Form.Row>
                <Form.Group as={Col} controlId="formGridCity">
                <Form.Label>City</Form.Label>
                <Form.Control placeholder = "Enter City"/>
                </Form.Group>

                <Form.Group as={Col} controlId="formGridState">
                <Form.Label>State</Form.Label>
                <Form.Control type="text" placeholder="Enter State" />
                </Form.Group>

                <Form.Group as={Col} controlId="formGridCountry">
                <Form.Label>Country</Form.Label>
                <Form.Control type="text" placeholder="Enter Country" />
                </Form.Group>
            </Form.Row>

            <Form.Group controlId="formGridDesc">
                <Form.Label>Work Description</Form.Label>
                <Form.Control as="textarea" placeholder= "Describe roles and responsibilities" />
            </Form.Group>

            <Button variant="primary" type="submit">
                Submit
            </Button>
            </Form>
        )
    }

    return (
        <Card bg="light">
            <Card.Body>
            <Card.Title>Work & Volunteer Experience</Card.Title>
               {content}
            </Card.Body>
            <Card.Footer>
            {!props.viewId && <Button variant="link" onClick = { () => props.changeExpMode(true) }>Add Work Experience</Button>}
            </Card.Footer>
        </Card>
    ); 

}