const mongoose = require('mongoose');
var Schema = mongoose.Schema;

const CompanySchema = new Schema({
  name: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    required: true,
    unique: true,
  },  
  password: {
    type: String,
    required: true,
  },
  description: {
    type: String,
  },
  city: {
    type: String,
  },
  state: {
    type: String,
  },
  country: {
    type: String,
  },
  profile_picture: {
    type: String,
  },
  phone_number: {
    type: String,
  },
  user: {
    type: String,
    default: 'company'
  },
  message_list: [
    {
      entity: {
        type: String
      },
      email: {
        type: String
      },
      date: {
        type: Date
      },
      message_conversation: [
        {
          body: {
            type: String,
          },
          date: {
            type: Date
          },
          action: {
            type: String
          },
        }
      ]
    }
  ],
});

module.exports = Company = mongoose.model('company', CompanySchema, 'company');