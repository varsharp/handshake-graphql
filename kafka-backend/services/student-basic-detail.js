var Student = require('../models/student');

handle_request =  async(msg, callback) => {   
    try {
        const student = await Student.findByIdAndUpdate(msg.id, msg);
        if(student) {
            callback(null, {status: 200, responseMessage: "Successful"});
        }
    } catch (e) {
        callback(null, {status: 500, responseMessage: 'Unable to save data.'});
    }
}

exports.handle_request = handle_request;
