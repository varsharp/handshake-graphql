var Job = require('../models/job');

handle_request =  async(msg, callback) => {
    try {
        const applications = await Job.find({ _id: msg.jobId }).select('application -_id');
        const students = [];
    
        if (applications && applications[0]) {
            for( const app of applications[0].application ) {
                let student = await Student.find({ _id: app.student });
                students.push(student[0]);
            }
            
            if(students && students.length > 0) {
                callback(null, {status: 200, responseMessage: students});
            }
        }
    
        callback(null, {status: 200, responseMessage: students});
    } catch (e) {
        callback(null, {status: 500, responseMessage: 'Unable to fetch data.'});
    }
}

exports.handle_request = handle_request;