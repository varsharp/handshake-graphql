var bcrypt = require('bcrypt');
var Company = require('../models/company');
const jwt = require('jsonwebtoken');
const config = require('../config');

handle_request =  async(msg, callback) => {
    const { email, password } = msg;
    try {
        const userCompany = await Company.findOne({
            email: email
        });
        if (!userCompany) {
            callback(null, {status: 400, responseMessage: { msg: 'Company not found.' }});
        }
        const isMatch = await bcrypt.compare(password.toString(), userCompany.password);
        if (!isMatch) {
            callback(null, {status: 400, responseMessage: { msg: 'Invalid Credentials. Please try again.' }});
        }
        const payload = {
            user: {
                _id: userCompany._id,
                email: userCompany.email,
				user: userCompany.user,
            },
        };
        jwt.sign(
            payload,
            config.JWTPASSWORD,
            {
                expiresIn: 360000,
            },
            (err, token) => {
                if (err) throw err;
                callback(null, {status: 200, responseMessage: {token: "JWT " + token, user: payload.user.user}});
            },
        );
    } catch (e) {
        callback(null, {status: 500, responseMessage: 'Unable to log in. Please try again.'});
    }
}

exports.handle_request = handle_request;