var bcrypt = require('bcrypt');
var Student = require('../models/student');
const jwt = require('jsonwebtoken');
const config = require('../config');

handle_request =  async(msg, callback) => {
	try {
        const student = msg;
        let userStudent = await Student.findOne({
                email: student.email,
		});
		
        if (userStudent) {
			// return res.status(404).send('Email already exists! Please sign in or create a new account.');
			callback(null, {status: 404, responseMessage: 'Email already exists! Please sign in or create a new account.'});
        }
        userStudent = new Student({
            ...student,
        });
        const salt = await bcrypt.genSalt(10);

        userStudent.password = await bcrypt.hash(userStudent.password, salt);
		await userStudent.save();        
        payload = {
            user: {
				_id: userStudent._id,
				email: userStudent.email,
				first_name: userStudent.first_name,
				user: userStudent.user,
            },
        };

        jwt.sign( payload, config.JWTPASSWORD, { expiresIn: 360000, },
            (err, token) => {               
                if (err) throw err;
				// res.status(200).end("JWT " + token);
				callback(null, {status: 200, responseMessage: {token: "JWT " + token, user: payload.user.user}});
            },
        );
    } catch (e) {
		// return res.status(500).json('Unable to sign up. Please try again.');
		callback(null, {status: 500, responseMessage: 'Unable to sign up. Please try again.'});
    }
}

exports.handle_request = handle_request;