var bcrypt = require('bcrypt');
var Company = require('../models/company');
const jwt = require('jsonwebtoken');
const config = require('../config');

handle_request =  async(msg, callback) => {
	try {        
        const company = msg;
        let userCompany = await Company.findOne({
                email: company.email,
		});
		
        if (userCompany) {
			// return res.status(404).send('Email already exists! Please sign in or create a new account.');
			callback(null, {status: 404, responseMessage: 'Email already exists! Please sign in or create a new account.'});
        }
        userCompany = new Company({
            ...company,
        });
        const salt = await bcrypt.genSalt(10);

        userCompany.password = await bcrypt.hash(userCompany.password, salt);
		await userCompany.save();        
        payload = {
            user: {
				_id: userCompany._id,
				email: userCompany.email,
				user: userCompany.user,
            },
        };

        jwt.sign( payload, config.JWTPASSWORD, { expiresIn: 360000, },
            (err, token) => {               
                if (err) throw err;
				// res.status(200).end("JWT " + token);
				callback(null, {status: 200, responseMessage: {token: "JWT " + token, user: payload.user.user}});
            },
        );
    } catch (e) {
		// return res.status(500).json('Unable to sign up. Please try again.');
		callback(null, {status: 500, responseMessage: 'Unable to sign up. Please try again.'});
    }
}

exports.handle_request = handle_request;