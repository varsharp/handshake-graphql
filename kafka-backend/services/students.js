const Student = require('../models/student');

handle_request = async (msg, callback) => {
    let options = {
        page: Number(msg.query.page),
        sort: {},
        limit: 5,
    }
    try {
        let student;
        if (msg.user.user === 'student') {
            student = await Student.findById(msg.user._id);
        }

        let students;
        if (!(msg.query.name || msg.query.college_name || msg.query.skill)) {
            students = await Student.paginate({_id : {'$ne': student._id}}, options, (err, result) => {
                return result.docs;
            })
        } else if (msg.query.name || msg.query.college_name || msg.query.skill) {
            if (msg.query.name) {
                students = await Student.paginate({first_name: msg.query.name}, options, (err, result) => {
                    return result.docs;
                })
            }
            if (msg.query.college_name) {
                students = await Student.paginate({college_name: msg.query.college_name}, options, (err, result) => {
                    return result.docs;
                })
            }
            if (msg.query.skill) {
                students = await Student.paginate({skills: [msg.query.skill]}, options, (err, result) => {
                    return result.docs;
                })
            }
        }
        callback(null, { status: 200, responseMessage: students });
    } catch (e) {
        callback(null, { status: 500, responseMessage: 'Unable to fetch data.' });
    }
};

exports.handle_request = handle_request;