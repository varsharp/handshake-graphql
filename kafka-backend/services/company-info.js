var Company = require('../models/company');

handle_request =  async(msg, callback) => {   
    try {
        const company = await Company.findByIdAndUpdate(msg.id, msg);
        if(company) {
            callback(null, {status: 200, responseMessage: "Successful"});
        }
    } catch (e) {
        callback(null, {status: 500, responseMessage: 'Unable to save data.'});
    }
}

exports.handle_request = handle_request;
